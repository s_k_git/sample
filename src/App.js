import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count : props.initialCount,
      txt : ""
    }

    this.kake = this.kake.bind(this)
    this.division = this.division.bind(this)
  }

  plus() {
    this.setState({
      count : this.state.count + 1
    })
  }

  minus() {
    this.setState({
      count : this.state.count - 1
    })
  }

  kake() {
    this.setState({
      count : this.state.count * this.state.count
    })
  }

  division() {
    this.setState({
      count : this.state.count / 2
    })
  }

  render() {
    return (
      <span>
        <div>
          count : {this.state.count}
        </div>

        <div>
          <input type="text" onChange={(e) => this.setState({txt : e.target.text})}/>
        </div>
        <div>
          <button onClick={this.plus.bind(this)}>Plus</button>
          <button onClick={this.minus.bind(this)}>Minus</button>
          <button onClick={this.kake}>Kake</button>
          <button onClick={this.division}>div</button>
        </div>
      </span>
    )
  }
}

export default App;
